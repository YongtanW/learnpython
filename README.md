# LearnPython


* [Learn_python](Learn_python.ipynb)

Python basic tutorial

* [Learn_python_norun](Learn_python_norun.ipynb)

Python basic tutorial with none cell run

# Installion python

Install python by official link $\Rightarrow$ [[python_link](https://www.python.org/downloads/)]

For MacOS/Linux may already installed python2 in the system, python 2 is stop updated and it will conflict with python 3. It also seems could not completed remove because some system library may still relay on python 2. 

For this reason, one software is recommanded -- [Anaconda](https://www.anaconda.com/) 

The advantages:

- 1. For all type system, Windows MacOS and Linux
- 2. Very easy difference version control
- 3. Included jupyter notbook/lab, no need other IDE

Install a module by Acaconda use commond: `conda install [packagename]`

Without Anaconda usually use `pip` commond to install module. Following `get-pip.py` is in current floder.

For ***Windows*** user:

Open powershell/cmd(admin):  py get-pip.py 

Make sure setup the environment variables

For ***MacOS*** user:

Open terminal: python get-pip.py

For ***Linux*** user:

Open terminal: python get-pip.py

After runing upper python scripts, then `pip install [packagename]`